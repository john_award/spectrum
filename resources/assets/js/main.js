
$('#sandwich').click(function(){
	$(this).closest('body').toggleClass('body__active');
});


$('.service-panel__arrow').click(function(){
	$(this).closest('.service-panel__btn-wrapper').toggleClass('service-panel__btn-wrapper--active');
});

$(function(){
	$(".user-tel").mask("+7(999) 999-9999");
});


$(document).ready(function() { 
	$('.btn--contact , .btn--phone').click( function(event){ 
		event.preventDefault(); 
		$('#overlay').fadeIn(400,
			function(){
				$('#modal_form') 
				.css('display', 'block')
				.animate({opacity: 1, top: '50%'}, 200); 
			});
	});
	/* Зaкрытие мoдaльнoгo oкнa */
	$('#modal_close, #overlay').click( function(){ 
		$('#modal_form')
		.animate({opacity: 0, top: '45%'}, 200,
			function(){ 
				$(this).css('display', 'none'); 
				$('#overlay').fadeOut(400); 
			}
			);
	});
});

$(document).ready(function() { 
	$('.btn--section--services').click( function(event){ 
		event.preventDefault(); 
		$('.about-services')
		.css('display', 'block')
		.animate({opacity: 1, top: '50%'}, 200); 
	});

	$('.about-services__close').click( function(){ 
		$('.about-services')
		.animate({opacity: 0, top: '45%'}, 200,
			function(){ 
				$(this).css('display', 'none'); 
			});
	});
});


