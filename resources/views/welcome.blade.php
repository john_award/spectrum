@extends("layouts.app")

	@auth
		@section("styles")
			{{ Html::style("css/admin.css") }}
		@endsection
	@endauth

	@section("contents")

		@include("blocks.sections.header")

		@include("blocks.sections.services")

		@include("blocks.sections.privileges")

		@include("blocks.sections.1263clients")

		@include("blocks.sections.steps")

		@include("blocks.sections.seo")

		@auth
			@include("blocks.modal_form_bg")
		@endauth
		<div id="overlay"></div>
	@endsection
