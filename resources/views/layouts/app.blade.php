<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="Expires" content="Sat, 11 Nov 2017 01:20:05 GMT">
	<title>SpectrumAudit</title>
	<meta name="keywords" content="">
	<meta name="description" content="">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	{{ Html::style('css/main.css') }}
	@yield("styles")
</head>
<body>
  @include("blocks.navbar")
  @yield("contents")
  @include("blocks.footer")
	@guest
  	@include("blocks.modal_form")
	@endguest
	@auth
		@include("blocks.modal_form_tel")
	@endauth
</body>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
{{ Html::script('js/main.js') }}
{{ Html::script('js/jquery.maskedinput.min.js') }}
{{ Html::script('js/sweetalert.js') }}

@auth
	{{ Html::script('js/admin.js') }}
@endauth

</html>
