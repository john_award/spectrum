<a name="privileges__link" ></a> <!-- что это за ссылка? -->
<section class="section section--privileges">

  <div class="container">
    <h2 class="section__title">
      Льготные программы

    </h2>
    <p class="section__text">
      Ниже представлены лишь некоторые программы, субсидии и льготы,<br> которые может получить начинающий предприниматель.
    </p>
    <div class="privileges">
      <div class="privileges__item">
        <img src="/img/privileges-icon1.png" class="privileges__icon"></img>
        <div class="privileges__box">
          <h2 class="privileges__title">Льготное кредитование</h2>
          <p class="privileges__text">Данный продукт предлагают практически все крупные банки России. Максимальная сумма, которую может получить бизнесмен, составляет 12 млн. руб.</p>
        </div>
      </div>
      <div class="privileges__item">
        <img src="/img/privileges-icon2.png" class="privileges__icon"></img>
        <div class="privileges__box">
          <h2 class="privileges__title">Льготная аренда</h2>
          <p class="privileges__text">Администрация отдельных городов может выделять предпринимателям помещения, которые находятся у нее на балансе с выгодными условиями аренды.</p>
        </div>
      </div>
      <div class="privileges__item">
        <img src="/img/privileges-icon3.png" class="privileges__icon"></img>
        <div class="privileges__box">
          <h2 class="privileges__title">Субсидирование сельско<wbr>хозяйственных</h2>
          <p class="privileges__text">Кроме денег на приобретение техники и оборудования,
          они также могут получить средства на покупку семян или племенного молодняка.</p>
        </div>
      </div>
      <div class="privileges__item">
        <img src="/img/privileges-icon4.png" class="privileges__icon"></img>
        <div class="privileges__box">
          <h2 class="privileges__title">Льготный лизинг</h2>
          <p class="privileges__text">Если объектом сделки было оборудование или техника, то государство может компенсировать часть выплаченных за него процентов - от одной до двух третей процентной выставки.</p>
        </div>
      </div>
      <div class="privileges__item">
        <img src="/img/privileges-icon5.png" class="privileges__icon"></img>
        <div class="privileges__box">
          <h2 class="privileges__title">Компенсация затрат на участие в ярмарках и выставках</h2>
          <p class="privileges__text">В этом случае участия на таких выставках можно вернуть до 2/3 расходов, понесенных на аренду и доставку оборудования, продукции, уплату регистрационного взноса.</p>
        </div>
      </div>
    </div>
    <div class="row">
      <a href="#" class="btn btn--section btn--phone">
        узнать больше
      </a>
    </div>

  </div>
</section>
