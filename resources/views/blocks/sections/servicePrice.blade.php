<section class="section section--form">
  <div class="container ">

    <div class="user-form__box">

      <form action="#" class="user-form">
        <h2 class="user-form__title" >Подберем тариф<br>индивидуально для вас</h2>
        <p class="user-form__text" >Заполните форму ниже, чтобы узнать стоимость своего тарифа</p>
        <div class="user-form__wrap-element">
          <div>
            <label for="user-name"></label>
            <input type="text" id="user-name" name="user-name" class="user-form__element"
            placeholder="Ваше имя" value="" style="background-image: url(/img/forma-user.png)" required/>
          </div>
          <div>
            <label for="user-phone"></label>
            <input type="tel" id="tel" name="user-phone" pattern="2[0-9]{3}-[0-9]{3}" placeholder="Ваш телефон" class="user-form__element user-tel" style="background-image: url(/img/forma-tel.png)" required/>
          </div>
          <input type="submit" id="submit-form" name="user-submit" class="btn user-form__btn" value="Узнать стоимость"/>
        </div>
      </form>
    </div>

  </div>
</section>
