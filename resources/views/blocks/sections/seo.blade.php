<section class="section section--gray">
  <div class="container">
    <h2 class="section__title">
      Полный перечень услуг
    </h2>
    <div class="service-panel">
      <div class="service-panel__sidebar">
        <a href="#">
          <div class="service-panel__btn-wrapper">
            <div class="service-panel__btn service-panel__btn__1">
              <p class="service-panel__text text text--light">Бухгалтерский учет</p>
              <a href="javascript:void(0)" class="service-panel__arrow"></a>

            </div>
            <ul class="service-panel__list">
              <li class="service-panel__item"><a href="#" class="service-panel__link">Pellentesque volutpat diam
              </a></li>
              <li class="service-panel__item"><a href="#" class="service-panel__link">Phasellus in orci non nisi</a></li>
              <li class="service-panel__item"><a href="#" class="service-panel__link">Nunc eget luctus nunc</a></li>
              <li class="service-panel__item"><a href="#" class="service-panel__link">Fusce bibendum quam massa,
              a fringilla metus</a></li>
            </ul>
          </div>
        </a>
        <a href="#">
          <div class="service-panel__btn-wrapper">
            <div class="service-panel__btn service-panel__btn__2">
              <p class="service-panel__text text text--light">Кадровый учет</p>
              <a href="javascript:void(0)" class="service-panel__arrow"></a>

            </div>
            <ul class="service-panel__list">
              <li class="service-panel__item"><a href="#" class="service-panel__link">Pellentesque volutpat diam
              </a></li>
              <li class="service-panel__item"><a href="#" class="service-panel__link">Phasellus in orci non nisi</a></li>
              <li class="service-panel__item"><a href="#" class="service-panel__link">Nunc eget luctus nunc</a></li>
              <li class="service-panel__item"><a href="#" class="service-panel__link">Fusce bibendum quam massa,
              a fringilla metus</a></li>
            </ul>
          </div>
        </a>
        <a href="#">
          <div class="service-panel__btn-wrapper">
            <div class="service-panel__btn service-panel__btn__3">
              <p class="service-panel__text text text--light">Начисление заработной<br>
              платы</p>
              <a href="javascript:void(0)" class="service-panel__arrow"></a>

            </div>
            <ul class="service-panel__list">
              <li class="service-panel__item"><a href="#" class="service-panel__link">Pellentesque volutpat diam
              </a></li>
              <li class="service-panel__item"><a href="#" class="service-panel__link">Phasellus in orci non nisi</a></li>
              <li class="service-panel__item"><a href="#" class="service-panel__link">Nunc eget luctus nunc</a></li>
              <li class="service-panel__item"><a href="#" class="service-panel__link">Fusce bibendum quam massa,
              a fringilla metus</a></li>
            </ul>
          </div>
        </a>
        <a href="#">
          <div class="service-panel__btn-wrapper">
            <div class="service-panel__btn service-panel__btn__4">
              <p class="service-panel__text text text--light">Бухгалтерский учет</p>
              <a href="javascript:void(0)" class="service-panel__arrow"></a>

            </div>
            <ul class="service-panel__list">
              <li class="service-panel__item"><a href="#" class="service-panel__link">Pellentesque volutpat diam
              </a></li>
              <li class="service-panel__item"><a href="#" class="service-panel__link">Phasellus in orci non nisi</a></li>
              <li class="service-panel__item"><a href="#" class="service-panel__link">Nunc eget luctus nunc</a></li>
              <li class="service-panel__item"><a href="#" class="service-panel__link">Fusce bibendum quam massa,
              a fringilla metus</a></li>
            </ul>
          </div>
        </a>
        <a href="#">
          <div class="service-panel__btn-wrapper">
            <div class="service-panel__btn service-panel__btn__5">
              <p class="service-panel__text text text--light">Бухгалтерский учет</p>
              <a href="javascript:void(0)" class="service-panel__arrow"></a>

            </div>
            <ul class="service-panel__list">
              <li class="service-panel__item"><a href="#" class="service-panel__link">Pellentesque volutpat diam
              </a></li>
              <li class="service-panel__item"><a href="#" class="service-panel__link">Phasellus in orci non nisi</a></li>
              <li class="service-panel__item"><a href="#" class="service-panel__link">Nunc eget luctus nunc</a></li>
              <li class="service-panel__item"><a href="#" class="service-panel__link">Fusce bibendum quam massa,
              a fringilla metus</a></li>
            </ul>
          </div>
        </a>




      </div>
      <div class="service-panel__content">
        <h2 class="service-panel__content__title">
          Nunc eget luctus nunc
        </h2>
        <p class="service-panel__content__text">
          Vivamus molestie dignissim vehicula. Maecenas rutrum finibus tellus. Morbi ac lacus at nunc scelerisque accumsan. Praesent non consectetur urna. Morbi eu viverra felis, ac congue dolor. Fusce lacus tellus, consectetur a gravida ultrices, sollicitudin in mauris. Aenean pharetra ante eget hendrerit mattis.
          <br><br>
          Pellentesque ut augue eget nisi tempus scelerisque sit amet nec neque. Vivamus dignissim elementum tortor, a faucibus nunc. Nam consequat elit sit amet orci tempor feugiat. Sed non turpis sit amet tellus maximus varius. Mauris placerat nisl a ex finibus, nec interdum odio aliquet. Curabitur euismod, magna eu euismod egestas, tellus metus interdum nulla, in sagittis eros mi eu odio. Sed elementum, ex a maximus lobortis, elit orci sagittis justo, quis sollicitudin turpis massa iaculis erat. In vel finibus leo. Sed elit urna, venenatis vitae turpis a, placerat ullamcorper eros. Nullam vehicula pellentesque mauris ac placerat. Nullam sed erat sed justo porta aliquam eu sit amet tellus. Sed placerat enim nec nisi vulputate, eu venenatis ligula blandit.
          <br><br>
          Donec condimentum purus purus. Ut sed eros in sapien eleifend pellentesque. Nunc scelerisque ligula sit amet ligula dapibus bibendum. Ut et velit eget quam consequat tincidunt. Duis vel interdum magna. Sed convallis sit amet sapien eu convallis. Duis porttitor, urna in vehicula lobortis, sapien erat hendrerit dolor, eu tristique arcu massa quis urna. Duis porta, elit eget interdum tristique, diam felis luctus mauris, in tristique mi lorem a purus. Nunc tristique tellus sit amet est tincidunt rutrum. Maecenas ultrices sapien eget nisl consectetur sollicitudin. Fusce tempor lobortis odio eu tincidunt. Nulla libero magna, faucibus eu sem aliquam, tristique consequat justo. Morbi a tincidunt nisi.
          <br><br>
          Vivamus viverra, nulla id semper aliquet, nulla ex egestas sem, eu pretium eros massa vel orci. Nulla accumsan malesuada pellentesque. Praesent eu fringilla magna. Nam ligula mi, volutpat id interdum sit amet, auctor nec diam. Sed accumsan pharetra nisi a placerat. In hac habitasse platea dictumst. Phasellus ac auctor risus. Aliquam porttitor, sem ut feugiat laoreet, arcu lectus posuere lacus, sit amet ultrices urna ligula quis ligula. Integer sed nunc arcu. Sed nunc turpis, congue sodales imperdiet faucibus, mattis et nibh. Ut vel felis a lorem sagittis egestas. Vestibulum luctus, lorem et scelerisque vestibulum, nibh neque euismod turpis, at mattis justo dui at nisl. Sed vitae nunc urna.
          <br><br>
        </p>
        <a href="#" class="service-panel__content__link " >Подробнее</a>
      </div>
    </div>
  </div>
</section>
