	<header class="section section--services">
		<div class="container">
			<div class="row row--services">
				<div class="row__cell">
					<div class="about-services">
						<span class="about-services__close">X</span>
						<p class="about-services__text about-services__text--first">
							Обработка первичной документациии с составлением бухгалтерских регистров, например: кассовая книга, книга продаж,книга покупок, авансовые отчеты, главная книга, книга учета доходов и расходов. А также составление всех форм отчетности.
						</p>
						<h2 class="about-services__title">
							Стоимость услуги
						</h2>
						<p class="about-services__price">
							всего от <span>700</span> до <span>9,000</span> ₽/мес.
						</p>
						<p class="about-services__text">
							* В рамках тарифа предусмотрены бесплатные выезды менеджера (клиентский или бизнес).
						</p>
						<p class="about-services__text about-services__text--mini">
							благодаря очень гибким тарифам - вы платите только за то, что вам нужно.
						</p>
					</div>
				</div>
				<div class="row__cell">
					<div class="services-page">
						<div class="services-page__box">
							<h1 class="services-page__title text--light">
								Бухгалтерский учет
							</h1>
							<p class="services-page__text text text--light text--large">
								Как для предприятия, так и для предпринимателя
							</p>
							<a href="#" class="btn btn--section btn--section--services">
								узнать больше
							</a>
						</div>
					</div>
				</div>

			</div>
		</div>
	</header>
