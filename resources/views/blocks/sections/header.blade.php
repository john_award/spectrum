<header class="section section--header">
  <div class="header container">
    @auth
      <img src="img/mountain.png" id="change_bg" style="margin-left: 90%" />
    @endauth
    <div class="header__box">
      <div class="header__title">
        @guest
        <h1 class="text--light">{!! $header[0]->contents !!}</h1>
        @endguest

        @auth
        <form class="user-form admin">
          <h1 style="width: 100%"><div id="contenteditable" contenteditable="true" class="editable text--light admin--input">{!! $header[0]->contents !!}</div></h1>
          <input type="hidden" name="id" value="{{ $header[0]->id }}" />
          <button type="submit" class="form--btn btn">Изменить</button>
        </form>
        @endauth
      </div>
      <div class="header__mini">
        @guest
        <p class="text text--large text--light">{{ $header_secondary[0]->contents }}</p>
        <a href="#" class="btn btn--header btn--phone">Оставить заявку</a>
        @endguest

        @auth
        <form class="user-form admin">
          <div contenteditable="true" class="editable text text--large text--light admin--input"><p>{{ $header_secondary[0]->contents }}</p></div>
          <input type="hidden" name="id" value="{{ $header_secondary[0]->id }}" />
          <button type="submit" class="form--btn btn">Изменить</button>
        </form>
        @endauth
      </div>

      @guest
      <p class="text text--light">{{ $header_asterisk[0]->contents }}</p>
      @endguest
      @auth
      <form class="user-form admin">
        <div contenteditable="true" class="editable text text--light admin--input"><p>{{ $header_asterisk[0]->contents }}</p></div>
        <input type="hidden" name="id" value="{{ $header_asterisk[0]->id }}" />
        <button type="submit" class="form--btn btn">Изменить</button>
      </form>
      @endauth
    </div>
    <div class="header__pointer text--light">выбрать услугу</div>
  </div>
</header>
