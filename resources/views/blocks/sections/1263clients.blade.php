<section class="section">
  <div class="container">
    <h2 class="section__title">
      Почему 1263 клиента выбрали нас?
    </h2>
    <div class="wherefore">
      <div class="wherefore__item-wrapper">
        <div class="wherefore__item">
          <div class="wherefore__icon" style="background-image: url(/img/wherefore-icon1.png)"></div>
          <h2 class="wherefore__title">12 лет на рынке<br>
          консалтинга</h2>
          <div class="wherefore__overlay">
            <p class="wherefore__text">Бухгалтерский учет, налоговое консультирование, юридические
            услуги и др.</p>
            <a href="#" class="btn__wherefore btn--phone">Подробнее</a>

          </div>
        </div>
      </div>
      <div class="wherefore__item-wrapper">
        <div class="wherefore__item">
          <div class="wherefore__icon" style="background-image: url(/img/wherefore-icon2.png)"></div>
          <h2 class="wherefore__title">От регистрации до <br>внешнеэкономической<br> деятельности</h2>
          <div class="wherefore__overlay">
            <p class="wherefore__text">Бухгалтерский учет, налоговое консультирование, юридические
            услуги и др.</p>
            <a href="#" class="btn__wherefore btn--phone">Подробнее</a>

          </div>
        </div>
      </div>
      <div class="wherefore__item-wrapper">
        <div class="wherefore__item">
          <div class="wherefore__icon" style="background-image: url(/img/wherefore-icon3.png)"></div>
          <h2 class="wherefore__title">Обширный спектр <br>услуг для бизнеса <br>в одном месте<br>
          консалтинга</h2>
          <div class="wherefore__overlay">
            <p class="wherefore__text">Бухгалтерский учет, налоговое консультирование, юридические
            услуги и др.</p>
            <a href="#" class="btn__wherefore btn--phone">Подробнее</a>

          </div>
        </div>
      </div>
      <div class="wherefore__item-wrapper">
        <div class="wherefore__item">
          <div class="wherefore__icon" style="background-image: url(/img/wherefore-icon4.png)"></div>
          <h2 class="wherefore__title">Мы заинтересованы<br> в развитии вашего<br> бизнеса.</h2>
          <div class="wherefore__overlay">
            <p class="wherefore__text">Бухгалтерский учет, налоговое консультирование, юридические
            услуги и др.</p>
            <a href="#" class="btn__wherefore btn--phone">Подробнее</a>

          </div>
        </div>
      </div>
      <div class="wherefore__item-wrapper">
        <div class="wherefore__item">
          <div class="wherefore__icon" style="background-image: url(/img/wherefore-icon5.png)"></div>
          <h2 class="wherefore__title">Разумные и гибкие <br>тарифы</h2>
          <div class="wherefore__overlay">
            <p class="wherefore__text">Бухгалтерский учет, налоговое консультирование, юридические
            услуги и др.</p>
            <a href="#" class="btn__wherefore btn--phone">Подробнее</a>

          </div>
        </div>
      </div>

    </div>
  </div>
</section>
