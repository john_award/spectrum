<section class="section section--steps-to-work">
  <div class="container">
    <h2 class="section__title">
      Всего 4 шага до результата!
    </h2>
    <div class="steps-to-work">
      <div class="steps-to-work__item">
        <div class="steps-to-work__block">
          <div class="steps-to-work__number">1 </div>
        </div>
        <div class="steps-to-work__box">
          <p class="steps-to-work__text">
            Вы оставляете заявку на нашем сайте или по телефону
          </p>
        </div>
      </div>
      <div class="steps-to-work__item">
        <div class="steps-to-work__block">
          <div class="steps-to-work__number">2</div>
        </div>
        <div class="steps-to-work__box">
          <p class="steps-to-work__text">
            Мы разбираем поставленную задачу
          </p>
        </div>
      </div>
      <div class="steps-to-work__item">
        <div class="steps-to-work__block">
          <div class="steps-to-work__number">3</div>
        </div>
        <div class="steps-to-work__box">
          <p class="steps-to-work__text">
            Согласовываем пути решения и подписываем договор
          </p>
        </div>
      </div>
      <div class="steps-to-work__item">
        <div class="steps-to-work__block">
          <div class="steps-to-work__number">4</div>
        </div>
        <div class="steps-to-work__box">
          <p class="steps-to-work__text">
            Вы получаете результат, выполненный профессионалами
          </p>
        </div>
      </div>
    </div>
    <div class="row">
      <a href="#" class="btn btn--section btn--phone">
        хочу результат
      </a>
    </div>
  </div>

</section>
