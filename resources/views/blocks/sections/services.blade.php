<a name="services-section__link"></a>
<section class="section">
  <div class="container">
    <h2 class="section__title">
      Услуги
    </h2>
    <div class="services">
      <div class="services__item">
        <a href="/services.html" class="services__item-link">
        <div class="services__icon  services__icon__1">
        </div>
        <h2 class="services__title">
          Бухгалтерский учет
        </h2>
        <p class="services__text">
          <br>
        </p>
      </a>
      </div>

      <div class="services__item">
        <a href="/services.html" class="services__item-link">
        <div class="services__icon  services__icon__2">
        </div>
        <h2 class="services__title">
          Кадровый учет
        </h2>
        <p class="services__text">
          <br>
        </p>
      </a>
      </div>
      <div class="services__item">
        <a href="/services.html" class="services__item-link">
        <div class="services__icon  services__icon__3">
        </div>
        <h2 class="services__title">
          Начисление<br>
          заработной платы
        </h2>
        <p class="services__text">
          <br>
        </p>
      </a>
      </div>
      <div class="services__item">
        <a href="/services.html" class="services__item-link">
        <div class="services__icon  services__icon__4">
        </div>
        <h2 class="services__title">
          Юридические услуги

        </h2>
        <p class="services__text">
          Регистрация предприятий,<br>
          правовые юридические услуги
        </p>
      </a>
      </div>
      <div class="services__item">
        <a href="/services.html" class="services__item-link">
        <div class="services__icon  services__icon__5">
        </div>
        <h2 class="services__title">
          Аудит
        </h2>
        <p class="services__text">
          <br>
        </p>
      </a>
      </div>
      <div class="services__item">
        <a href="/services.html" class="services__item-link">
        <div class="services__icon  services__icon__6">
        </div>
        <h2 class="services__title">
          Консультационные услуги
        </h2>
        <p class="services__text">
          <br>
        </p>
      </a>
      </div>
      <div class="services__item">
        <a href="/services.html" class="services__item-link">
        <div class="services__icon  services__icon__7">
        </div>
        <h2 class="services__title">
          Налоговый «консультант»
        </h2>
        <p class="services__text">
          <br>
        </p>
      </a>
      </div>
      <div class="services__item">
        <a href="/services.html" class="services__item-link">
        <div class="services__icon  services__icon__8">
        </div>
        <h2 class="services__title">
          Внешнеэкономическая<br>
          деятельность
        </h2>
        <p class="services__text">
          <br>
        </p>
      </a>
      </div>
      <div class="services__item">
        <a href="/services.html" class="services__item-link">
        <div class="services__icon  services__icon__9">
        </div>
        <h2 class="services__title">
          Услуги курьерской <br> и почтово-консультантской<br>
          службы
        </h2>
        <p class="services__text">
          <br>
        </p>
      </a>
      </div>


    </div>


    <div class="row">
      <a href="#" class="btn btn--section btn--phone">
        дополнительные услуги
      </a>
    </div>
  </div>
</section>
