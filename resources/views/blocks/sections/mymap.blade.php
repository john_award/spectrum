<section class="section section--gray section--map">
  <div id="map" class="mymap">
    <div class="container">
      <div class="mymap__box">
        <h2 class="mymap__title">
          контакты и адреса
        </h2>
        <div class="mymap__text-block">
          <p class="mymap__text">
            <strong>Телефоны:</strong>
          </p>
          <p class="mymap__text">

            +7 (863) 239-94-20<br>8 (800) 123-12-12

          </p>
        </div>
        <div class="mymap__text-block">
          <p class="mymap__text">
            <strong>E-mail:</strong>
          </p>
          <p class="mymap__text">

            buh@audit-spektr.com

          </p>
        </div>
        <div class="mymap__text-block">
          <p class="mymap__text">
            <strong>Режим работы:</strong>
          </p>
          <p class="mymap__text">

            пн-пт с 9:00 до 18:00

          </p>
        </div>
        <div class="mymap__text-block">
          <p class="mymap__text">
            <strong>Адрес:</strong>
          </p>
          <p class="mymap__text">

            344000, Россия,<br>
            г. Ростов-на-Дону,<br>
            ул. Красноармейская,<br>
            д. 65/87, оф. 101
          </p>
        </div>
        <a href="#" class="btn btn--contact  btn--contact--map">Перезвоните мне</a>
      </div>
    </div>
  </div>
</section>
