<nav class="menu--fixed">
  <div class="container">
    <div class="menu">
      <a href="{{ Route('welcome') }}" class="menu__logo"></a>
      <ul class="menu__list">
        <li class="menu__item"><a href="{{ Route('welcome') }}" class="menu__link">Главная</a></li>
        <!-- <li class="menu__item"><a href="/index.html#services-section__link" class="menu__link">Услуги</a></li> -->
        <li class="menu__item"><a href="{{ Route('welcome'). '#services-section__link' }}" class="menu__link">Услуги</a></li>
        <li class="menu__item"><a href="{{ Route('rates') }}" class="menu__link">Тарифы</a></li>
        <!-- <li class="menu__item"><a href="/index.html#privileges__link" class="menu__link">Льготные программы</a></li> -->
        <li class="menu__item"><a href="{{ Route('welcome'). '#privileges__link' }}" class="menu__link">Льготные программы</a></li>
        <li class="menu__item"><a href="{{ Route('contact') }}" class="menu__link">Контакты</a></li>
      </ul>
      <div class="menu__box">
        <div class="menu__contact">
          <div class="contact__mobile">
            {{ $tel[0]->contents }}
            
          </div>
          <div class="contact__time">
            {{ $working_hours[0]->contents }}
          </div>
        </div>
        @guest
        <a href="#" class="btn btn--contact">Перезвоните мне</a>
        @endguest
      </div>
      <a href="javascript:void(0)" class="sandwich"  id="sandwich"></a>
    </div>
    <nav class="sandwich-menu">
      <ul class="sandwich-menu__list">
        <li class="sandwich-menu__item"><a href="/index.html" class="sandwich-menu__link">Главная</a></li>
        <li class="sandwich-menu__item"><a href="/index.html#services-section__link" class="sandwich-menu__link">Услуги</a></li>
        <li class="sandwich-menu__item"><a href="/rates.html" class="sandwich-menu__link">Тарифы</a></li>
        <li class="sandwich-menu__item"><a href="/index.html#privileges__link" class="sandwich-menu__link">Льготные программы</a></li>
        <li class="sandwich-menu__item"><a href="/contact.html" class="sandwich-menu__link">Контакты</a></li>




      </ul>
      <div class="sandwich-menu__box">
        <div class="menu__contact">
          <div class="contact__mobile">
            {{ $tel[0]->contents }}

          </div>
          <div class="contact__time">
            {{ $working_hours[0]->contents }}
          </div>
        </div>
        <a href="#" class="btn btn--contact">Перезвоните мне</a>
      </div>
    </nav>

  </div>
</nav>
