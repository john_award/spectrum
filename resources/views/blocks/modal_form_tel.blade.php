<div id="modal_form_tel">
		<span class="modal_close" id="modal_close">X</span>
		<form class="user-form admin" method="POST">
					<meta name="csrf-token" content="{{ csrf_token() }}">
					<h2 class="user-form__title" >Основной номер телефона Вашей<br>компании</h2>
					<p class="user-form__text" >Данный номер будет отображаться в навигации сайта</p>
					<div class="user-form__wrap-element">
						<div>
							<label for="tel"></label>
							<input type="hidden" name="id" value="{{ $tel[0]->id }}" />
							<input type="tel" id="tel" name="tel" pattern="2[0-9]{3}\-[0-9]{3}" class="tel user-form__element user-tel" value="{{ $tel[0]->contents }}" required/>
						</div>
						<input type="submit" id="submit-form" name="user-submit" class="btn user-form__btn" value="Сохранить"/>
					</div>
		</form>
		<form class="user-form admin" style="margin-top: 30px" method="POST">
					<meta name="csrf-token" content="{{ csrf_token() }}">
					<h2 class="user-form__title" style="margin-top: 20px; margin-bottom: 20px">Режим работы<br>Спектр Аудит</h2>
					<div class="user-form__wrap-element">
						<div>
							<input type="hidden" name="id" value="{{ $working_hours[0]->id }}" />
							<label for="tel"></label>
							<input type="text" id="working_hours" name="working_hours" class="working_hours user-form__element" value="{{ $working_hours[0]->contents }}" required/>
						</div>
						<input type="submit" id="submit-form" name="user-submit" class="btn user-form__btn" value="Сохранить"/>
					</div>
		</form>
	</div>
