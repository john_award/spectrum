@extends("layouts.app")

	@section("contents")

		@include("blocks.sections.serviceHero")

		@include("blocks.sections.servicePrice")

		@include("blocks.sections.seo")

	@endsection

	<div id="overlay"></div>


	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<script src="/js/jquery.maskedinput.min.js"></script>
	<script src="/js/main.js"></script>
</body>
</html>
