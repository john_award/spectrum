<html>
<head>
  <title>Страница не найдена</title>
  <style>
    div {
      height: 100vh;
      display: grid;
      align-items: center;
    }
    h1 {
      text-align: center;
      font-size: 100px;
    }
  </style>
</head>
<body>
  <div>
    <h1>404</h1>
  </div>
</body>
</html>
