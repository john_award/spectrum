@extends('layouts.app')

	@section("contents")
		@include("blocks.sections.mymap")
	@endsection

	<div id="overlay"></div>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
	<script src="/js/jquery.maskedinput.min.js"></script>
	<script type="text/javascript">

		ymaps.ready(init);
		var myMap, myPlacemark;
		function init(){
			var center = [47.22922022, 39.70803798];
			var mql = window.matchMedia('all and (max-width: 639px)');
			if (mql.matches) {
				center = [47.23381755, 39.71456530];
			}
			myMap = new ymaps.Map("map", {
				center: center,
				zoom: 16
			});
			myPlacemark = new ymaps.Placemark([47.22879634, 39.71468986], {}, {
				iconLayout: 'default#image',
				iconImageHref: '/img/iconmap.png',
				iconImageSize: [350, 220],
				iconImageOffset: [-170, -170]
			});
			myMap.geoObjects.add(myPlacemark);
		}
	</script>
</body>
</html>
