<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Data extends Model
{
    protected $table = "text_number_data";
    protected $fillable = ["contents"];
}
