<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Data;


class adminController extends Controller
{

    public function __construct()
    {
      $this->middleware('auth');
    }

    // just update the current record
    public function changeSection(Request $request) {
      $d = Data::find($request->id);
      //in editable div a new line is make with a div
      //so just replace all divs with <br /> and it's ok)
      $contents = str_replace("</div>", "", $request->contents);
      $contents = str_replace("<div>", "<br>", $request->contents);
      $d->contents = strip_tags($contents, "<br>");
      $d->save();
      return redirect()->route("welcome");
    }
}
