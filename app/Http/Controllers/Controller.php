<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use App\Http\Requests\Request;

// models
use DB;
use App\Customer;
use App\Data;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function welcome() {
      $header = Data::where("section", "header")->get();
      $header_secondary = Data::where("section", "header_secondary")->get();
      $header_asterisk = Data::where("section", "header_asterisk")->get();
      $tel = Data::where("section", "tel")->get();
      $working_hours = Data::where("section", "working_hours")->get();
      return view("welcome", compact("header", "header_secondary", "header_asterisk",
      "tel", "working_hours"));
    }

    public function addCustomerData(Request $request) {
      $customer = new customer;

      $this -> validate($request, array(
        'user_name' => 'required|string|max:255',
        'user_phone' => 'required|string'
      ));

      $customer->user_name = $request->user_name;
      $customer->user_phone = $request->user_phone;
      $customer->save();
      return view('welcome', compact('user_phone', $request->user_phone));

      // return redirect()->route('welcome');
    }

    public function services() {
      return view("services");
    }

    public function rates() {
      return view("rates");
    }

    public function privileges__link() {
      return view("privileges__link");
    }

    public function contact() {
      return view("contact");
    }
}
