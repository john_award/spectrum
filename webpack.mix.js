let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.react('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/_buttons.scss', 'public/css')
   .sass('resources/assets/sass/_dev.scss', 'public/css')
   .sass('resources/assets/sass/_fonts.scss', 'public/css')
   .sass('resources/assets/sass/_grid.scss', 'public/css')
   .sass('resources/assets/sass/_mixins.scss', 'public/css')
   .sass('resources/assets/sass/main.scss', 'public/css')
   .js('resources/assets/js/main.js', 'public/js');
