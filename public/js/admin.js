$(document).ready(function() {

  $('.user-form').submit(function(e) {
    var data, id, contents, user=0, url;

    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    e.preventDefault();
    var type = 'POST';
    if ($(this).hasClass('admin')) {
      // data for ajax request
      id = $(this).find('input').val(); // element id (hidden)
      // check if it's a tel or not
      if ($(this).find('.tel').length > 0) {
        contents = $(this).find('.tel').val();
      }
      if ($(this).find('.working_hours').length > 0) {
        contents = $(this).find('.working_hours').val();
      }
      else {
        contents = $(this).find('.editable').html(); // element contents
      }
      data = {'id': id, 'contents': contents};
      url = './change_section';
      swal({
        title: 'Записать изменения',
        text: 'Изменения будут записаны в базу данных',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Да ',
        cancelButtonText: 'Нет',
        confirmButtonClass: 'btn form--btn modals--btn',
        cancelButtonClass: 'btn form--btn modals--btn',
        buttonsStyling: false
      }).then(function () {
        swal(
          'Изменения внесены',
          'Изменения внесены в базу данных',
          'success'
        );

      }, function (dismiss) {
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if (dismiss === 'cancel') {
          swal(
            'Отмена',
            'Изменения не были внесены',
            'error'
          )
        }
      })
    }
    else { // it is a user
      url = './call_me';
      data = $(this).serialize();
    }
    $.ajax({
      type: type,
      url: url,
      data: data,
      success: function() {
        if (url != './change_section') {
          $('#modal_form').animate({ opacity: 0, top: '45%' }, 200, function () {
      			$(this).css('display', 'none');
      			$('#overlay').fadeOut(400);
      		});
          $('.user-form').trigger('reset');
        }
        else {
          $('#modal_form_tel')
          .animate({opacity: 0, top: '45%'}, 200,
            function(){
              $(this).css('display', 'none');
              $('#overlay').fadeOut(400);
            }
            );
            if (contents.startsWith('+')) {
              $('.contact__mobile').text(contents);
            }
            if (contents.startsWith('Пн') || contents.startsWith('ПН') || contents.startsWith('пн')) {
              console.log(contents);
              $('.contact__time').text(contents);
            }
        }
      }
    })
  })


  var textarea = document.querySelector('textarea');

  // textarea.addEventListener('keydown', autosize);

  function autosize() {
    var el = this;
    setTimeout(function(){
      el.style.cssText = 'height:auto; padding:0';
      el.style.cssText = 'height:' + el.scrollHeight + 'px';
    },0);
  }

  // make a submit buttn appear from nowhere when the editable is active
  $('.admin--input').focus(function() {
    $(this).closest('.admin').find('.form--btn').animate({
      opacity: 1,
      paddingTop: '24px',
      paddingBottom: '24px'

    }, 400, function() {})
  })

  $('.admin--input').focusout(function() {
    $(this).closest('.admin').find('.form--btn').animate({
      opacity: 0,
      paddingTop: '0px',
      paddingBottom: '0px'

    }, 400, function() {})
  })

  function openModal(click, modal) {
    $(click).click( function(event){
    event.preventDefault();
    $('#overlay').fadeIn(400,
      function(){
        $(modal)
        .css('display', 'block')
        .animate({opacity: 1, top: '50%'}, 200);
      });
    });
  }

  function closeModal(click, modal) {
    $(click).click( function(){
      $(modal)
      .animate({opacity: 0, top: '45%'}, 200,
        function(){
          $(this).css('display', 'none');
          $('#overlay').fadeOut(400);
        }
        );
      });
  }

  openModal('.menu__contact', '#modal_form_tel');
  openModal('#change_bg', '#modal_form_change_bg');


/* Зaкрытие мoдaльнoгo oкнa */
closeModal('.modal_close', '#modal_form_tel');


})
