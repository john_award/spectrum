<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get("/", "Controller@welcome")->name("welcome");
// customer form
Route::post('/call_me', 'Controller@addCustomerData')->name('call_me ');


Route::get("/services", "Controller@services")->name("services");
Route::get("/rates", "Controller@rates")->name("rates");
Route::get("/contact", "Controller@contact")->name("contact");

/* Registration is not needed here, so just
 *  comment out 'Auth' route and add only routes that we need
 *  from vendor\laravel\framework\src\Illuminate\Routing\Router.php
 */

//Auth::routes();

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/home', 'HomeController@index')->name('home');

// Routes for manipulation with text, backgorund and db data
// 1. change header title
Route::post("/change_section", "adminController@changeSection")->name("change_section");
